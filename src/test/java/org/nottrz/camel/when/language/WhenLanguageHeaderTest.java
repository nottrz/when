package org.nottrz.camel.when.language;

import static org.nottrz.camel.when.active.RangeTestUtils.newWeekDay;

import java.util.Date;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import org.nottrz.camel.when.active.RangeTestUtils;

public class WhenLanguageHeaderTest extends CamelTestSupport {

	static {
		RangeTestUtils.setLocale("en");
	}
	
    @Produce(uri = "direct:start")
    protected ProducerTemplate template;
    
	private String timeExpression = "MON-FRI";
	
    @Test
    public void testMonday() throws Exception {

    	Date monday = newWeekDay("MON");
    	template.sendBodyAndHeader("any body", "when.date", monday);
    	
        assertThisExchanges(1);
    }

    @Test
    public void testSunday() throws Exception {
    	
    	Date sunday = newWeekDay("SUN");
    	template.sendBodyAndHeader("any body", "when.date", sunday);
    	
        assertThisExchanges(0);
    }
    
	private void assertThisExchanges(int count) throws InterruptedException {
		MockEndpoint mock = getMockEndpoint("mock:result");
        mock.expectedMinimumMessageCount(count);
        assertMockEndpointsSatisfied();
	}

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new RouteBuilder() {
            @Override
			public void configure() {
				from("direct:start")
                  .filter("when", timeExpression)
                  .to("mock:result");
            }
        };
    }

}
