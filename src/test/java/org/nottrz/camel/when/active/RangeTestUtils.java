package org.nottrz.camel.when.active;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class RangeTestUtils {

	private static Locale locale = Locale.getDefault();
	
	public static void setLocale(String newLocale) {
		locale = Locale.forLanguageTag(newLocale);
	}
	
	public static Date newDay(String string) throws ParseException {

		SimpleDateFormat sdf = newSDFormat("dd-MM-yyyy HH:mm");
		return sdf.parse(string);
	}

	public static Date newTime(String string) throws ParseException {

		SimpleDateFormat sdf = newSDFormat("HH:mm");
		return sdf.parse(string);
	}

	public static Date newWeekDay(String string) throws ParseException {
		SimpleDateFormat sdf = newSDFormat("E");
		return sdf.parse(string);
	}
	
	public static Date newWeekTime(String string) throws ParseException {
		SimpleDateFormat sdf = newSDFormat("E HH:mm");
		return sdf.parse(string);
	}
	
	private static SimpleDateFormat newSDFormat(String pattern) {
		return new SimpleDateFormat(pattern, locale);
	}
	
	public static Date newDateFromNow(String exp) throws ParseException {
		Calendar c = Calendar.getInstance();
		
		String signStr = exp.charAt(0)+"";
		int sign = signStr.equals("+") ? 1 : -1;
		
		exp = exp.substring(1);
		String[] split = exp.split("\\W");
		String unit = split[1];
		int amount = Integer.parseInt(split[0]);
		if (unit.equals("days")) {
			c.add(Calendar.DAY_OF_YEAR, sign * amount);
		}
		else if (unit.equals("hours")) {
			c.add(Calendar.HOUR, sign * amount);
		}
		else if (unit.equals("minutes")) {
			c.add(Calendar.MINUTE, sign * amount);
		}
		return c.getTime();
	}

}
