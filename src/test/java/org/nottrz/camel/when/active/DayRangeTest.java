package org.nottrz.camel.when.active;

import static org.junit.Assert.*;
import static org.nottrz.camel.when.active.RangeTestUtils.newDay;

import java.util.Date;

import org.junit.Test;

public class DayRangeTest {

	static {
		RangeTestUtils.setLocale("it");
	}

	@Test
	public void testName() throws Exception {

		Date d = newDay("27-9-2012 13:20"); // giovedi'

		assertNotSuspended(d, "");	// match all
		assertNotSuspended(d, "GIOV");
		assertNotSuspended(d, "LUN-GIOV");
		assertNotSuspended(d, "GIOV-VEN");

		assertNotSuspended(d, "SAB-VEN");	// loop around

		assertSuspended(d, "LUN");
		assertSuspended(d, "LUN-MAR");
		assertSuspended(d, "DOM-MERC");
		assertSuspended(d, "VEN-MERC");	// loop around
	}

	private void assertNotSuspended(Date d, String s) {
		DayRange r = new DayRange(s, Days.createDays("IT"));
		assertTrue(r.matchDate(d));
	}

	private void assertSuspended(Date d, String s) {
		DayRange r = new DayRange(s, Days.createDays("IT"));
		assertFalse(r.matchDate(d));
	}
}
