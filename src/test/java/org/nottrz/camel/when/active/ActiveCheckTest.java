package org.nottrz.camel.when.active;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.nottrz.camel.when.active.RangeTestUtils.*;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

public class ActiveCheckTest {

	static {
		RangeTestUtils.setLocale("it");
	}
	
	private ActiveCheck check;
	
	@Test
	public void timeRanges() throws ParseException {

		Date d = newTime("13:20");

		assertActive(d, "");
		assertSuspended(d, "13:30-15:30");
		assertSuspended(d, "11:00-13:00,15:30-18:00");

		assertActive(d, "11:00-15:00");
		// left inclusive
		assertActive(d, "11:00-13:20,15:30-18:00");
		// right inclusive
		assertActive(d, "13:20-18:00");
		// matches second part (with overlap)
		assertActive(d, "11:00-13:00,11:30-18:00");

		// two minutes to midnight...
		d = newTime("23:58");
		assertActive(d, "19:00-23:59, 00:00-7:00");
		d = newTime("00:00");
		assertActive(d, "19:00-23:59, 00:00-7:00");
		d = newTime("00:07");
		assertActive(d, "19:00-23:59, 00:00-7:00");

		// loop around midnight
		d = newTime("23:07");
		assertActive(d, "19:00-7:00");
		d = newTime("3:07");
		assertActive(d, "19:00-7:00");

		// negated times
		d = newTime("3:07");
		assertSuspended(d, "!19:00-7:00");
		d = newTime("15:07");
		assertActive(d, "!19:00-7:00");

		// negated times with hole
		d = newTime("22:07");
		assertSuspended(d, "!19:00-20:00, 22:00-23:00");
		d = newTime("21:07");
		assertActive(d, "!19:00-20:00, 22:00-23:00");
	}

	@Test
	public void hoursWithoutMinutes() throws ParseException {

		Date d = newTime("13:20");
		assertActive(d, "13-15");
		assertActive(d, "13:10-15");
		assertActive(d, "13-15:30");
		assertSuspended(d, "13:30-15");
	}

	@Test
	public void timeWithWeekDays() throws ParseException {

		Date giovedi = newWeekTime("GIO 13:20");

		assertActive(giovedi, "GIOV");
		assertSuspended(giovedi, "LUN");

		assertActive(giovedi, "LUN-VEN");
		assertActive(giovedi, "LUN,GIOV");

		assertActive(giovedi, "GIOV 11:00-15:00");
		assertActive(giovedi, "LUN-VEN 11:00-15:00");
		assertSuspended(giovedi, "LUN 11:00-15:00");

		assertSuspended(giovedi, "LUN,MAR 11:00-15:00");
		assertActive(giovedi, "LUN,GIOV 11:00-13:20, 15:30-18:00");
		assertSuspended(giovedi, "!LUN,GIOV 11:00-13:20, 15:30-18:00");
	}

	@Test
	public void holeInTheMiddle() throws ParseException {
		
		String expr = "LUN-VEN 9-18; !MERC 12-16";
		
		Date lunediMattina = newWeekTime("LUN 16:00");
		assertActive(lunediMattina, expr);
		
		Date mercMattina = newWeekTime("MER 11:00");
		assertActive(mercMattina, expr);
		
		Date mercNoon = newWeekTime("MER 13:00");
		assertSuspended(mercNoon, expr);

		Date mercPom = newWeekTime("MER 17:00");
		assertActive(mercPom, expr);
		
		Date venerdiMattino = newWeekTime("VEN 10:20");
		assertActive(venerdiMattino, expr);
		
		Date sabatoMattino = newWeekTime("SAB 10:20");
		assertSuspended(sabatoMattino, expr);
	}

	@Test
	public void complexCase() throws ParseException {

		// the !DOM part is actually superfluous
		String expr = "LUN-VEN 9-14, 20:30-01:30; SAB 9-13:30; !DOM";

		Date lunediMattina = newWeekTime("LUN 16:00");
		assertSuspended(lunediMattina, expr);

		Date lunediPom = newWeekTime("LUN 16:00");
		assertSuspended(lunediPom, expr);

		Date lunediNotte = newWeekTime("LUN 23:00");
		assertActive(lunediNotte, expr);
		Date lunediNotte2 = newWeekTime("LUN 00:00");
		assertActive(lunediNotte2, expr);
		Date lunediNotte3 = newWeekTime("LUN 01:15");
		assertActive(lunediNotte3, expr);
		Date lunediNotte4 = newWeekTime("LUN 01:45");
		assertSuspended(lunediNotte4, expr);

		Date sabatoMattino = newWeekTime("SAB 10:20");
		assertActive(sabatoMattino, expr);

		Date sabatoPom = newWeekTime("SAB 16:20");
		assertSuspended(sabatoPom, expr);

		Date domenica = newWeekTime("DOM 11:20");
		assertSuspended(domenica, expr);
	}

	@Test(expected=IllegalArgumentException.class)
	public void failForInvalidText() throws ParseException {
		new ActiveCheck("not SUN or SAT at 21:00");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void failForInvaliDays() throws ParseException {
		new ActiveCheck("DRZ");
	}

	@Test(expected=IllegalArgumentException.class)
	public void failForInvalidTimes() throws ParseException {
		new ActiveCheck("33:24-11:45");
	}
	
	@Test
	public void parserShouldNotBeTrickedBySpacesInTheDayPart() throws ParseException {

		new ActiveCheck("LUN, MAR 11:00-15:00", "IT");
	}
	

	private void assertSuspended(Date d, String suspensionRanges) {
		check = new ActiveCheck(suspensionRanges, "IT");
		assertFalse("Date: " + d + ", ranges:" + suspensionRanges, check.isActive(d));
	}

	private void assertActive(Date d, String suspensionRanges) {
		check = new ActiveCheck(suspensionRanges, "IT");
		assertTrue("Date: " + d + ", ranges:" + suspensionRanges, check.isActive(d));
	}

}
