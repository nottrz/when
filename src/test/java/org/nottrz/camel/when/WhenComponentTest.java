package org.nottrz.camel.when;


import java.util.Date;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import org.nottrz.camel.when.active.RangeTestUtils;

public class WhenComponentTest extends CamelTestSupport {

	static {
		RangeTestUtils.setLocale("en");
	}

	private String timeExpression = "MON-FRI";
	
    @Test
    public void testMonday() throws Exception {

    	Date monday = RangeTestUtils.newWeekDay("MON");
		TimeSource.freezeTimeAt(monday);
    	
        assertThisExchanges(1);
    }

    @Test
    public void testSunday() throws Exception {
    	
    	Date sunday = RangeTestUtils.newWeekDay("SUN");
		TimeSource.freezeTimeAt(sunday);
    	
        assertThisExchanges(0);
    }
    
	private void assertThisExchanges(int count) throws InterruptedException {
		MockEndpoint mock = getMockEndpoint("mock:result");
        mock.expectedMinimumMessageCount(count);
        assertMockEndpointsSatisfied();
	}

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new RouteBuilder() {
            @Override
			public void configure() {
				from("timer://foo?delay=0")
                  .to("when:" + timeExpression)
                  .to("mock:result");
            }
        };
    }

}
