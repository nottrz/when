package org.nottrz.camel.when;

import java.util.Date;

public class TimeSource {

	private static Date frozenDate;

	public static Date now() {
		return frozenDate != null? frozenDate : new Date();
	}
	
	public static void freezeTimeAt(Date date) {
		frozenDate = date;
	}
}
