package org.nottrz.camel.when.language;

import org.apache.camel.Expression;
import org.apache.camel.IsSingleton;
import org.apache.camel.Predicate;
import org.apache.camel.spi.Language;

public class WhenLanguage implements Language, IsSingleton {

	@Override
	public Predicate createPredicate(String expression) {
		return new WhenExpression(this, expression, Boolean.class);
	}

	@Override
	public Expression createExpression(String expression) {
		return new WhenExpression(this, expression, Object.class);
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}