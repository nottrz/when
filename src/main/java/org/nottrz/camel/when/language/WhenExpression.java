package org.nottrz.camel.when.language;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.support.ExpressionSupport;
import org.nottrz.camel.when.TimeSource;
import org.nottrz.camel.when.active.ActiveCheck;

public class WhenExpression extends ExpressionSupport {

    private static final String HEADER_WHEN_DATE = "when.date";
    
	private final String expressionString;
    private final Class<?> type;
    private ActiveCheck activeCheck;
    
    public WhenExpression(WhenLanguage rangeLanguage, String expressionString, Class<?> type) {
    	
        this.expressionString = expressionString;
        this.type = type;
        this.activeCheck = new ActiveCheck(expressionString, null);
	}

	public static WhenExpression range(String expression) {
        return new WhenExpression(new WhenLanguage(), expression, Object.class);
    }
    
	@Override
	public <T> T evaluate(Exchange exchange, Class<T> tClass) {
		
		Date now = exchange.getIn().getHeader(HEADER_WHEN_DATE, Date.class);
		if (now == null) {
			now = TimeSource.now();
		}
		
        boolean value = activeCheck.isActive(now);
		return exchange.getContext().getTypeConverter().convertTo(tClass, value);
	}

    public Class<?> getType() {
        return type;
    }
    
	@Override
	protected String assertionFailureMessage(Exchange exchange) {
		return expressionString;
	}

    @Override
    public String toString() {
        return "Range[" + expressionString + "]";
    }
}
