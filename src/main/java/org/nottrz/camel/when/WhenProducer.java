/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.nottrz.camel.when;


import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultProducer;
import org.nottrz.camel.when.active.ActiveCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe che riceve un Exchange da fuori e lo processa.
 *
 *
 */
public class WhenProducer extends DefaultProducer {

    private static final Logger log = LoggerFactory.getLogger(WhenProducer.class);

    private WhenEndpoint endpoint;

    private ActiveCheck activeCheck;

    /*
     * Serve per loggare una volta sola il momento in cui il servizio entra in
     * una fascia sospesa.
     */
    private boolean onLastCallWasActive = true;

    public WhenProducer(WhenEndpoint endpoint) {
        super(endpoint);
        this.endpoint = endpoint;

        activeCheck = new ActiveCheck(endpoint.getTimeExpression(), endpoint.getLocale());
    }

    @Override
	public void process(Exchange exchange) throws Exception {

    	Date now = TimeSource.now();
		if (!activeCheck.isActive(now)) {
    		if (onLastCallWasActive) {
    			log.info("Stopping requests. Time range expression: " + endpoint.getTimeExpression());
    			onLastCallWasActive = false;
    		}
    		exchange.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
    	}
    	else {
    		if (!onLastCallWasActive) {
    			log.debug("Accepting requests. Time range expression: " + endpoint.getTimeExpression());
    			onLastCallWasActive = true;
    		}
    	}
    }

}
