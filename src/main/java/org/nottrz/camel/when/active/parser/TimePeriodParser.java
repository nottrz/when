// Generated from /home/trz/progetti/LegalBus/trunk/camel/components/DMS-component/src/main/java/it/infocert/legalbus/dbpoller/active/parser/TimePeriod.g4 by ANTLR 4.1
package org.nottrz.camel.when.active.parser;

    import java.util.*;
    //import it.infocert.legalbus.dbpoller.active.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TimePeriodParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, COMMA=2, RANGE_SEP=3, EXPR_SEP=4, NOT=5, WORD=6, TIME_PART=7, 
		NUM=8, WS=9;
	public static final String[] tokenNames = {
		"<INVALID>", "':'", "','", "'-'", "';'", "'!'", "WORD", "TIME_PART", "NUM", 
		"WS"
	};
	public static final int
		RULE_exprList = 0, RULE_expr = 1, RULE_dayPart = 2, RULE_dayList = 3, 
		RULE_negation = 4, RULE_day = 5, RULE_dayRange = 6, RULE_timeList = 7, 
		RULE_timeRange = 8, RULE_time = 9;
	public static final String[] ruleNames = {
		"exprList", "expr", "dayPart", "dayList", "negation", "day", "dayRange", 
		"timeList", "timeRange", "time"
	};

	@Override
	public String getGrammarFileName() { return "TimePeriod.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public TimePeriodParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ExprListContext extends ParserRuleContext {
		public ExprContext expr;
		public List<ExprContext> vals = new ArrayList<ExprContext>();
		public List<TerminalNode> EXPR_SEP() { return getTokens(TimePeriodParser.EXPR_SEP); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode EXPR_SEP(int i) {
			return getToken(TimePeriodParser.EXPR_SEP, i);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ExprListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitExprList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprListContext exprList() throws RecognitionException {
		ExprListContext _localctx = new ExprListContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_exprList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(20); ((ExprListContext)_localctx).expr = expr();
			((ExprListContext)_localctx).vals.add(((ExprListContext)_localctx).expr);
			setState(25);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==EXPR_SEP) {
				{
				{
				setState(21); match(EXPR_SEP);
				setState(22); ((ExprListContext)_localctx).expr = expr();
				((ExprListContext)_localctx).vals.add(((ExprListContext)_localctx).expr);
				}
				}
				setState(27);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public DayPartContext dayPart() {
			return getRuleContext(DayPartContext.class,0);
		}
		public TimeListContext timeList() {
			return getRuleContext(TimeListContext.class,0);
		}
		public NegationContext negation() {
			return getRuleContext(NegationContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_expr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(29);
			_la = _input.LA(1);
			if (_la==NOT) {
				{
				setState(28); negation();
				}
			}

			setState(32);
			_la = _input.LA(1);
			if (_la==WORD) {
				{
				setState(31); dayPart();
				}
			}

			setState(35);
			_la = _input.LA(1);
			if (_la==TIME_PART) {
				{
				setState(34); timeList();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DayPartContext extends ParserRuleContext {
		public DayListContext dayList() {
			return getRuleContext(DayListContext.class,0);
		}
		public DayRangeContext dayRange() {
			return getRuleContext(DayRangeContext.class,0);
		}
		public DayPartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dayPart; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitDayPart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DayPartContext dayPart() throws RecognitionException {
		DayPartContext _localctx = new DayPartContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_dayPart);
		try {
			setState(39);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(37); dayRange();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(38); dayList();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DayListContext extends ParserRuleContext {
		public DayContext day;
		public List<DayContext> vals = new ArrayList<DayContext>();
		public List<DayContext> day() {
			return getRuleContexts(DayContext.class);
		}
		public DayContext day(int i) {
			return getRuleContext(DayContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(TimePeriodParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(TimePeriodParser.COMMA, i);
		}
		public DayListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dayList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitDayList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DayListContext dayList() throws RecognitionException {
		DayListContext _localctx = new DayListContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_dayList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41); ((DayListContext)_localctx).day = day();
			((DayListContext)_localctx).vals.add(((DayListContext)_localctx).day);
			setState(46);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(42); match(COMMA);
				setState(43); ((DayListContext)_localctx).day = day();
				((DayListContext)_localctx).vals.add(((DayListContext)_localctx).day);
				}
				}
				setState(48);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NegationContext extends ParserRuleContext {
		public TerminalNode NOT() { return getToken(TimePeriodParser.NOT, 0); }
		public NegationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_negation; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitNegation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NegationContext negation() throws RecognitionException {
		NegationContext _localctx = new NegationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_negation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(49); match(NOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DayContext extends ParserRuleContext {
		public TerminalNode WORD() { return getToken(TimePeriodParser.WORD, 0); }
		public DayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_day; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitDay(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DayContext day() throws RecognitionException {
		DayContext _localctx = new DayContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_day);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51); match(WORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DayRangeContext extends ParserRuleContext {
		public TerminalNode RANGE_SEP() { return getToken(TimePeriodParser.RANGE_SEP, 0); }
		public List<DayContext> day() {
			return getRuleContexts(DayContext.class);
		}
		public DayContext day(int i) {
			return getRuleContext(DayContext.class,i);
		}
		public DayRangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dayRange; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitDayRange(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DayRangeContext dayRange() throws RecognitionException {
		DayRangeContext _localctx = new DayRangeContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_dayRange);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53); day();
			setState(54); match(RANGE_SEP);
			setState(55); day();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeListContext extends ParserRuleContext {
		public TimeRangeContext timeRange;
		public List<TimeRangeContext> vals = new ArrayList<TimeRangeContext>();
		public List<TerminalNode> COMMA() { return getTokens(TimePeriodParser.COMMA); }
		public TimeRangeContext timeRange(int i) {
			return getRuleContext(TimeRangeContext.class,i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(TimePeriodParser.COMMA, i);
		}
		public List<TimeRangeContext> timeRange() {
			return getRuleContexts(TimeRangeContext.class);
		}
		public TimeListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitTimeList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TimeListContext timeList() throws RecognitionException {
		TimeListContext _localctx = new TimeListContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_timeList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57); ((TimeListContext)_localctx).timeRange = timeRange();
			((TimeListContext)_localctx).vals.add(((TimeListContext)_localctx).timeRange);
			setState(62);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(58); match(COMMA);
				setState(59); ((TimeListContext)_localctx).timeRange = timeRange();
				((TimeListContext)_localctx).vals.add(((TimeListContext)_localctx).timeRange);
				}
				}
				setState(64);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeRangeContext extends ParserRuleContext {
		public TimeContext from;
		public TimeContext to;
		public TerminalNode RANGE_SEP() { return getToken(TimePeriodParser.RANGE_SEP, 0); }
		public List<TimeContext> time() {
			return getRuleContexts(TimeContext.class);
		}
		public TimeContext time(int i) {
			return getRuleContext(TimeContext.class,i);
		}
		public TimeRangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeRange; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitTimeRange(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TimeRangeContext timeRange() throws RecognitionException {
		TimeRangeContext _localctx = new TimeRangeContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_timeRange);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65); ((TimeRangeContext)_localctx).from = time();
			setState(66); match(RANGE_SEP);
			setState(67); ((TimeRangeContext)_localctx).to = time();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeContext extends ParserRuleContext {
		public Token hour;
		public Token min;
		public TerminalNode TIME_PART(int i) {
			return getToken(TimePeriodParser.TIME_PART, i);
		}
		public List<TerminalNode> TIME_PART() { return getTokens(TimePeriodParser.TIME_PART); }
		public TimeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_time; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TimePeriodVisitor ) return ((TimePeriodVisitor<? extends T>)visitor).visitTime(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TimeContext time() throws RecognitionException {
		TimeContext _localctx = new TimeContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_time);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69); ((TimeContext)_localctx).hour = match(TIME_PART);
			setState(72);
			_la = _input.LA(1);
			if (_la==1) {
				{
				setState(70); match(1);
				setState(71); ((TimeContext)_localctx).min = match(TIME_PART);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3\13M\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\3"+
		"\2\3\2\3\2\7\2\32\n\2\f\2\16\2\35\13\2\3\3\5\3 \n\3\3\3\5\3#\n\3\3\3\5"+
		"\3&\n\3\3\4\3\4\5\4*\n\4\3\5\3\5\3\5\7\5/\n\5\f\5\16\5\62\13\5\3\6\3\6"+
		"\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\7\t?\n\t\f\t\16\tB\13\t\3\n\3\n\3"+
		"\n\3\n\3\13\3\13\3\13\5\13K\n\13\3\13\2\f\2\4\6\b\n\f\16\20\22\24\2\2"+
		"J\2\26\3\2\2\2\4\37\3\2\2\2\6)\3\2\2\2\b+\3\2\2\2\n\63\3\2\2\2\f\65\3"+
		"\2\2\2\16\67\3\2\2\2\20;\3\2\2\2\22C\3\2\2\2\24G\3\2\2\2\26\33\5\4\3\2"+
		"\27\30\7\6\2\2\30\32\5\4\3\2\31\27\3\2\2\2\32\35\3\2\2\2\33\31\3\2\2\2"+
		"\33\34\3\2\2\2\34\3\3\2\2\2\35\33\3\2\2\2\36 \5\n\6\2\37\36\3\2\2\2\37"+
		" \3\2\2\2 \"\3\2\2\2!#\5\6\4\2\"!\3\2\2\2\"#\3\2\2\2#%\3\2\2\2$&\5\20"+
		"\t\2%$\3\2\2\2%&\3\2\2\2&\5\3\2\2\2\'*\5\16\b\2(*\5\b\5\2)\'\3\2\2\2)"+
		"(\3\2\2\2*\7\3\2\2\2+\60\5\f\7\2,-\7\4\2\2-/\5\f\7\2.,\3\2\2\2/\62\3\2"+
		"\2\2\60.\3\2\2\2\60\61\3\2\2\2\61\t\3\2\2\2\62\60\3\2\2\2\63\64\7\7\2"+
		"\2\64\13\3\2\2\2\65\66\7\b\2\2\66\r\3\2\2\2\678\5\f\7\289\7\5\2\29:\5"+
		"\f\7\2:\17\3\2\2\2;@\5\22\n\2<=\7\4\2\2=?\5\22\n\2><\3\2\2\2?B\3\2\2\2"+
		"@>\3\2\2\2@A\3\2\2\2A\21\3\2\2\2B@\3\2\2\2CD\5\24\13\2DE\7\5\2\2EF\5\24"+
		"\13\2F\23\3\2\2\2GJ\7\t\2\2HI\7\3\2\2IK\7\t\2\2JH\3\2\2\2JK\3\2\2\2K\25"+
		"\3\2\2\2\n\33\37\"%)\60@J";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}