
grammar TimePeriod;

@header {
    import java.util.*;
    //import it.infocert.legalbus.dbpoller.active.*;
}

// parser
     
exprList: vals+=expr (EXPR_SEP vals+=expr)*;

expr: negation? dayPart? timeList?;

dayPart: dayRange | dayList;

dayList: vals+=day (COMMA vals+=day)*;

negation: NOT;

day: WORD;

dayRange: day RANGE_SEP day;

timeList: vals+=timeRange (COMMA vals+=timeRange)*;
            
timeRange: from=time RANGE_SEP to=time;

time: hour=TIME_PART (':' min=TIME_PART)?;



// lexer

COMMA: ',';

RANGE_SEP: '-';

EXPR_SEP: ';';

NOT: '!';

WORD: [a-zA-Z]+;

TIME_PART: [0-6]? NUM;

NUM: [0-9];

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

