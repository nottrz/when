// Generated from /home/trz/progetti/LegalBus/trunk/camel/components/DMS-component/src/main/java/it/infocert/legalbus/dbpoller/active/parser/TimePeriod.g4 by ANTLR 4.1
package org.nottrz.camel.when.active.parser;

    import java.util.*;
    //import it.infocert.legalbus.dbpoller.active.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TimePeriodParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TimePeriodVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#negation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegation(@NotNull TimePeriodParser.NegationContext ctx);

	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#time}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTime(@NotNull TimePeriodParser.TimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#timeRange}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeRange(@NotNull TimePeriodParser.TimeRangeContext ctx);

	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#timeList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeList(@NotNull TimePeriodParser.TimeListContext ctx);

	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#dayPart}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayPart(@NotNull TimePeriodParser.DayPartContext ctx);

	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#dayRange}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayRange(@NotNull TimePeriodParser.DayRangeContext ctx);

	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(@NotNull TimePeriodParser.ExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#day}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDay(@NotNull TimePeriodParser.DayContext ctx);

	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#dayList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayList(@NotNull TimePeriodParser.DayListContext ctx);

	/**
	 * Visit a parse tree produced by {@link TimePeriodParser#exprList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprList(@NotNull TimePeriodParser.ExprListContext ctx);
}