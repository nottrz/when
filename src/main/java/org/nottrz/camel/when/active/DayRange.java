package org.nottrz.camel.when.active;


import static org.nottrz.camel.when.active.SimpleUtils.*;

import java.util.Calendar;
import java.util.Date;

public class DayRange implements DateMatcher {

	private static final int FIRST_DAY = 0;
	private static final int LAST_DAY = 6;

	private final int from;
	private final int to;

	public DayRange(String from, String to, Days days) {
		this.from = days.indexOfDay(from);
		this.to = days.indexOfDay(to);
	}

	public DayRange(String dayExpr, Days days) {

		if (isBlank(dayExpr)) {
			this.from = FIRST_DAY;
			to = LAST_DAY;
		} else {
			String[] parts = splitAndTrim(dayExpr, "-");
			this.from = days.indexOfDay(parts[0]);
			this.to = parts.length > 1 ? days.indexOfDay(parts[1]) : from;
		}
	}

	@Override
	public boolean matchDate(Date d) {

		int day = dayOfWeek(d);

		boolean dayMatches;
		if (from <= to) {
			dayMatches = inRange(from, to, day);
		} else {
			dayMatches = inRange(from, LAST_DAY, day) || inRange(FIRST_DAY, to, day);
		}
		return dayMatches;
	}

	static int dayOfWeek(Date d) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		return day;
	}

	static boolean inRange(int idxFrom, int idxTo, int day) {
		return day >= idxFrom && day <= idxTo;
	}

}
