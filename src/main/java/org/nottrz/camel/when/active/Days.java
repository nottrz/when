package org.nottrz.camel.when.active;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

public class Days {

	private String[] dayNames;

	public Days(String[] dayNames) {
		this.dayNames = dayNames;
	}

	int indexOfDay(String dayName) {

		for (int i = 0; i < dayNames.length; i++) {
			String name = dayNames[i];
			if (name.startsWith(dayName)) {
				return i;
			}
		}
		String localeDescr = " (Default values are based on JVM locale : " + Locale.getDefault() + ". Override these with the 'camel.when.locale' system property or with the 'locale' parameter)";
		throw new IllegalArgumentException("Day not found '" + dayName + "'. Valid days are: " + Arrays.toString(dayNames) + localeDescr + ".");
	}
	
	public static Days createDays(String localeStr) {

		Locale locale = LocaleUtils.findBestLocale(localeStr);
		
		final long JUST_ONE_SUNDAY = 1383506147637L;
		String[] dayNames = new String[7];
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(JUST_ONE_SUNDAY);
		for (int i = 0; i < dayNames.length; i++) {
			String dayName = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, locale);
			dayNames[i] = dayName.toUpperCase();
			cal.add(Calendar.DAY_OF_YEAR, 1);
		}
		return new Days(dayNames);
	}


}
