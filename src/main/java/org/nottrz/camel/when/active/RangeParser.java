package org.nottrz.camel.when.active;


import static org.nottrz.camel.when.active.SimpleUtils.*;

import java.util.ArrayList;
import java.util.List;

public class RangeParser {

	DateMatcher parseDayPart(String dateExpr, Days days) {

		if (isBlank(dateExpr)) {
			return DateMatcher.MATCH_ALL;
		}

		if(dateExpr.contains(",")) {
			return new DayListRange(dateExpr.trim(), days);
		}
		return new DayRange(dateExpr, days);
	}

	public List<TimeRange> parseTimeParts(String fullTimePart) {

		List<TimeRange> timeRanges = new ArrayList();

		if (isBlank(fullTimePart)) {
			return timeRanges;
		}

		String timePart = fullTimePart;

		String[] timeRange = splitAndTrim(timePart, ",");
		for (int i = 0; i < timeRange.length; i++) {
			String[] split = timeRange[i].split("-");
			if (split.length != 2) {
				throw new IllegalArgumentException("Time range should respect this format: hh:mm-hh:mm. Found " + timePart);
			}
			int from = toMinutes(split[0]);
			int to = toMinutes(split[1]);
			timeRanges.add(new TimeRange(from, to));
		}
		return timeRanges;
	}

	private static int toMinutes(String hourAndMinutes) {

		String[] split = splitAndTrim(hourAndMinutes, ":");

		int hour = Integer.parseInt(split[0]);
		validateHours(hour);
		int min = 0;
		if (split.length > 1) {
			min = Integer.parseInt(split[1]);
			validateMinutes(min);
		}
		return TimeRange.toMinutes(hour, min);
	}
}
