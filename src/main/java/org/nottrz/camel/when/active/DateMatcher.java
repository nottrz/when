package org.nottrz.camel.when.active;


import java.util.Date;

public interface DateMatcher {

	public static DateMatcher MATCH_ALL = new DateMatcher() {
	
		@Override
		public boolean matchDate(Date d) {
			return true;
		}
	};

	boolean matchDate(Date d);

}
