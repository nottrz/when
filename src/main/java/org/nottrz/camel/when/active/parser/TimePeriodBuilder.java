package org.nottrz.camel.when.active.parser;


import static org.nottrz.camel.when.active.SimpleUtils.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.nottrz.camel.when.active.*;
import org.nottrz.camel.when.active.parser.TimePeriodParser.DayContext;
import org.nottrz.camel.when.active.parser.TimePeriodParser.DayListContext;
import org.nottrz.camel.when.active.parser.TimePeriodParser.DayPartContext;
import org.nottrz.camel.when.active.parser.TimePeriodParser.DayRangeContext;
import org.nottrz.camel.when.active.parser.TimePeriodParser.ExprContext;
import org.nottrz.camel.when.active.parser.TimePeriodParser.TimeListContext;
import org.nottrz.camel.when.active.parser.TimePeriodParser.TimeRangeContext;

public class TimePeriodBuilder {

    public static void main(String[] args) throws Exception {

    	String text = "!DOM; LUN-VEN 9-14, 20:30-01:30; SAB 9-14:30";
    	//String text = "!DOM-SAB";

    	new TimePeriodBuilder().parse(text, "IT");
    }

    public List<Range> parse(String text, String locale) {

    	TimePeriodLexer lexer = new TimePeriodLexer(new ANTLRInputStream(text));
    	TimePeriodParser parser = new TimePeriodParser(new CommonTokenStream(lexer));
		parser.getErrorListeners().clear();
		CustomErrorListener err = new CustomErrorListener(text);
		parser.addErrorListener(err);
    	
        ParseTree tree = parser.exprList();
        
        validateErrors(parser, text, err);
        
        BuilderVisitor visitor = new BuilderVisitor(Days.createDays(locale));
        visitor.visit(tree);

        return visitor.ranges;
    }

	private void validateErrors(TimePeriodParser parser, String text, CustomErrorListener err) {
		int errors = parser.getNumberOfSyntaxErrors();
        if (errors > 0) {
        	throw new IllegalArgumentException(err.errorMessage);
        }
	}

	static class BuilderVisitor extends TimePeriodBaseVisitor {

    	List<Range> ranges = new ArrayList<Range>();
		private final Days days;

    	public BuilderVisitor(Days days) {
			this.days = days;
		}
    	
		@Override
    	public Range visitExpr(ExprContext ctx) {

			boolean negated = ctx.negation() != null ? true : false;

			DateMatcher dayRange = DateMatcher.MATCH_ALL;
			if (ctx.dayPart() != null) {
				dayRange = (DateMatcher) this.visit(ctx.dayPart());
			}

    		List<TimeRange> timeRanges = Collections.EMPTY_LIST;
    		if (ctx.timeList() != null) {
    			timeRanges = (List<TimeRange>) this.visit(ctx.timeList());
    		}

    		org.nottrz.camel.when.active.Range range = new org.nottrz.camel.when.active.Range(dayRange, timeRanges, negated);
			ranges.add(range);
			return range;
    	}

		@Override
		public Object visitDayPart(DayPartContext ctx) {

			DayRangeContext dayRange = ctx.dayRange();
			if (dayRange != null) {
				return this.visit(dayRange);
			} else {
				return this.visit(ctx.dayList());
			}
		}

		@Override
		public Object visitDayRange(DayRangeContext ctx) {
			return new DayRange(ctx.getChild(0).getText(), ctx.getChild(2).getText(), days);
		}

		@Override
		public DayListRange visitDayList(DayListContext ctx) {

			List dayNames = new ArrayList();
			List<DayContext> vals = ctx.vals;
			for (DayContext dayContext : vals) {
				dayNames.add(dayContext.getText());
			}
			return new DayListRange(dayNames, days);
		}

		@Override
		public List<TimeRange> visitTimeList(TimeListContext ctx) {
			List timeRanges = new ArrayList();
			List<TimeRangeContext> vals = ctx.vals;
			for (TimeRangeContext timeRangeContext : vals) {
				timeRanges.add(this.visit(timeRangeContext));
			}
			return timeRanges;
		}

		@Override
		public TimeRange visitTimeRange(TimeRangeContext ctx) {

			int fromHour = parseHour(ctx.from.hour);
			int fromMin = parseMinutesOrZero(ctx.from.min);
			int toHour = parseHour(ctx.to.hour);
			int toMin = parseMinutesOrZero(ctx.to.min);

			return new TimeRange(fromHour, fromMin, toHour, toMin);
		}

		private int parseHour(Token hourToken) {
			int hour = Integer.parseInt(hourToken.getText());
			validateHours(hour);
			return hour;
		}

		private int parseMinutesOrZero(Token min) {
			if (min == null) {
				return 0;
			}
			String text = min.getText();
			int minutes = Integer.parseInt(text);
			validateMinutes(minutes);
			return text != null ? minutes : 0;
		}

    }

    private final class CustomErrorListener extends BaseErrorListener {
    	
    	String errorMessage = null;
    	String text;

    	public CustomErrorListener(String text) {
			this.text = text;
		}
    	
		@Override
		public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg,
				RecognitionException e) {

			if (errorMessage != null) {
				return;	// ignore subsequent errors
			}
			
			errorMessage = "Error parsing query. ";
			if (offendingSymbol instanceof CommonToken) {

				Token t = (Token) offendingSymbol;

				StringBuffer sb = new StringBuffer(text);
				sb.insert(t.getStopIndex() + 1, " <<<< ");
				sb.insert(t.getStartIndex(), " >>>> ");
				
				errorMessage +=  "Check this for an hint: '" + sb.toString() + "'";
			}
			else {
				errorMessage += "Error at character " + charPositionInLine + ": " + msg;
			}
		}
	}
}
