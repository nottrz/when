package org.nottrz.camel.when.active;

import java.util.Calendar;
import java.util.Date;

public class TimeRange implements DateMatcher {

	private static final int FIRST_MINUTE = 0;
	private static final int LAST_MINUTE = 60 * 24;

	private final int from;
	private final int to;

	public TimeRange(int from, int to) {
		this.from = from;
		this.to = to;
	}

	public TimeRange(int fromHour, int fromMin, int toHour, int toMin) {
		from = TimeRange.toMinutes(fromHour, fromMin);
		to = TimeRange.toMinutes(toHour, toMin);
	}

	@Override
	public boolean matchDate(Date d) {

		int minutes = minutesSinceMidnight(d);
		boolean matches;
		if (rangeIncludesMidnight()) {
			matches = inRange(from, to, minutes);
		} else {
			matches = inRange(from, LAST_MINUTE, minutes) || inRange(FIRST_MINUTE, to, minutes);
		}
		return matches;
	}

	private boolean rangeIncludesMidnight() {
		return from <= to;
	}

	private int minutesSinceMidnight(Date d) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);
		
		int minutesValue = TimeRange.toMinutes(hour, min);
		return minutesValue;
	}

	static int toMinutes(int hour, int min) {
		return hour * 60 + min;
	}

	static boolean inRange(int idxFrom, int idxTo, int day) {
		return day >= idxFrom && day <= idxTo;
	}


}
