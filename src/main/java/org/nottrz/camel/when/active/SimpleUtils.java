package org.nottrz.camel.when.active;


public class SimpleUtils {

	public static boolean isBlank(String s) {
		return s == null || s.trim().length() == 0;
	}
	
	public static String[] splitAndTrim(String s, String c) {
		String regex = "\\s*" + c + "\\s*";
		String[] attributes = s.split(regex);
		return attributes;
	}
	
	public static void validateHours(int val) {
		validateRange("hour", val, 0, 23);
	}

	public static void validateMinutes(int val) {
		validateRange("minutes", val, 0, 59);
	}

	private static void validateRange(String descr, int val, int from, int to) {
		if (val < from || val > to) {
			throw new IllegalArgumentException("Value " + val +  " for " + descr + " is invalid. Should be inside the range: " + from + "-" + to);
		}
	}

}
