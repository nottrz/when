package org.nottrz.camel.when.active;


import static java.lang.Character.isDigit;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Range {

	private static RangeParser rangeParser = new RangeParser();

	private final List<TimeRange> timeRanges;
	private final DateMatcher daysRange;
	private boolean negated;

	public Range(List<TimeRange> timeRanges, DateMatcher daysRange) {
		this(daysRange, timeRanges, false);
	}

	public Range(DateMatcher daysRange, List<TimeRange> timeRanges, boolean negated) {
		this.timeRanges = timeRanges;
		this.daysRange = daysRange;
		this.negated = negated;
	}

	boolean matchDate(Date d) {

		if (daysRange.matchDate(d) && matchTime(d)) {
			return true;
		}
		return false;
	}


	public boolean matchTime(Date d) {

		if (timeRanges.isEmpty()) {
			return true;
		}

		for (Iterator<TimeRange> iterator = timeRanges.iterator(); iterator.hasNext();) {
			TimeRange range = iterator.next();
			if (range.matchDate(d)) {
				return true;
			}
		}
		return false;

	}

	static Range stringToRange(String rangeStr, String locale) {

		rangeStr = rangeStr.trim();
		boolean isNegated = false;
		if (rangeStr.startsWith("!")) {
			isNegated = true;
			rangeStr = rangeStr.substring(1);
		}

		String dayPart = extractDayPart(rangeStr);
		String timePart = rangeStr.substring(dayPart.length()).trim();

		Range range = new Range(rangeParser.parseTimeParts(timePart), readDayPart(dayPart, locale));
		range.negated = isNegated;

		return range;
	}

	private static DateMatcher readDayPart(String dayPart, String locale) {
		return rangeParser.parseDayPart(dayPart, Days.createDays(locale));
	}

	private static String extractDayPart(String range) {

		int idx = dayPartEndIndex(range);
		if (idx > 0) {
			return range.substring(0, idx);
		}
		return "";
	}

	private static int dayPartEndIndex(String range) {
		for (int i = 0; i < range.length(); i++) {
			char c = range.charAt(i);
			if (isDigit(c)) {
				return i-1;
			}
		}
		return range.length();
	}

	public boolean isNegated() {
		return negated;
	}
}
