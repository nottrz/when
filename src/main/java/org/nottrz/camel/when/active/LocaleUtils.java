package org.nottrz.camel.when.active;

import java.util.Locale;

public class LocaleUtils {

	static Locale findLocale(String localeStr) {
		if (localeStr == null) {
			return null;
		}
		return Locale.forLanguageTag(localeStr);
	}
	
	public static Locale findBestLocale(String localeStr) {
		Locale locale;
		if (localeStr != null) {
			locale = findLocale(localeStr);
		}
		else {
			locale = findLocale(System.getProperty("when.camel.locale"));
			if (locale == null) {
				locale = Locale.getDefault();
			}
		}
		return locale;
	}

}
