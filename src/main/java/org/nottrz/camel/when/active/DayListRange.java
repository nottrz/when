package org.nottrz.camel.when.active;


import static org.nottrz.camel.when.active.SimpleUtils.splitAndTrim;

import java.util.Date;
import java.util.List;

public class DayListRange implements DateMatcher {

	private final int[] activeDays;

	public DayListRange(String expr, Days days) {

		String[] daysStr = splitAndTrim(expr, ",");
		activeDays = new int[daysStr.length];
		for (int i = 0; i < daysStr.length; i++) {
			String dayName = daysStr[i];
			activeDays[i] = days.indexOfDay(dayName);
		}
	}

	public DayListRange(List<String> dayNames, Days days) {
		
		activeDays = new int[dayNames.size()];
		for (int i = 0; i < dayNames.size(); i++) {
			String day = dayNames.get(i);
			activeDays[i] = days.indexOfDay(day);
		}
	}

	@Override
	public boolean matchDate(Date d) {

		int day = DayRange.dayOfWeek(d);
		for (int i = 0; i < activeDays.length; i++) {
			if (day == activeDays[i]) {
				return true;
			}
		}
		return false;
	}

}
