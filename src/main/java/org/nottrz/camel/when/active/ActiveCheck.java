package org.nottrz.camel.when.active;


import static org.nottrz.camel.when.active.SimpleUtils.splitAndTrim;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.nottrz.camel.when.active.parser.TimePeriodBuilder;

/**
 *
 * I periodi in cui il servizio e' ATTIVO.
 *
 * Pseudo-grammatica delle espressioni valide:
 *
 * EXPR_LIST := EXPR*  (separate da ;)
 *
 * EXPR := [!] [DAY_PART] [TIME_PART*]
 *
 * DAY_PART := DAY_RANGE | DAY_LIST
 *
 * DAY_RANGE := GIORNO1-GIORNO2  (inclusi, gestione "passaggio della mezzanotte", senza spazi)
 *
 * DAY_LIST := GIORNO1,GIORNO2,...   (senza spazi)
 *
 * TIME_PART := HH[:mm]-HH[:mm],...     (inclusi, separati da virgola, gestione "mezzanotte")
 *
 *
 *
 *
 * I giorni della settimana sono matchati per prefisso di 3 lettere: LUN, LUNE,
 * LUNEDI', ecc. sono tutti equivalenti.
 *
 *
 * Esempi:
 *
 * !DOM; LUN-VEN 9-14, 20:30-01:30; SAB 9-14:30
 *
 * LUN,MER,GIOV		(equivalente a !MAR,VEN,SAB,DOM oppure !MAR;!VEN-DOM)
 *
 * !00:30-01:30
 *
 * Vedi test per altri casi.
 *
 * L'ordine delle espressioni non e' significativo. Le espressioni vengono combinate in
 * questo modo:
 *
 *  - le espressioni negative devono passare tutte
 *  - se ci sono espressioni positive, deve essercene almeno una valida
 *
 */
public class ActiveCheck {

	private List<Range> list;

	public ActiveCheck(String activeRanges) {
		this(activeRanges, Locale.getDefault().getCountry());
	}

	public ActiveCheck(String activeRanges, String locale) {
		//list = parse(activeRanges, locale);
		list = simpleParse(activeRanges, locale);
	}

	public boolean isActive(Date d) {

		int positiveChecks = 0;
		for (Range range : list) {
			if (range.matchDate(d)) {
				if (range.isNegated()) {
					return false;
				} else {
					positiveChecks++;
				}
			}
		}

		if (areAllNegated(list)) {
			return true;
		}
		return positiveChecks > 0;
	}

	private boolean areAllNegated(List<Range> list) {
		for (Range range : list) {
			if (!range.isNegated()) {
				return false;
			}
		}
		return true;
	}

	private List<Range> parse(String ranges, String locale) {

    	return new TimePeriodBuilder().parse(ranges, locale);
	}
	
    private List<Range> simpleParse(String ranges, String locale) {
    	
		String[] string = splitAndTrim(ranges, ";");
		List<Range> result = new ArrayList<Range>();
		for (int i = 0; i < string.length; i++) {
			String rangeStr = string[i];
			Range range = Range.stringToRange(rangeStr, locale);
			result.add(range);
		}
		return result;
	}

}
